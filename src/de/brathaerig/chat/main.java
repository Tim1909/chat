/*
 *All rights reserved by Tim Brathärig
 *Copyright by Tim Brathärig 2015
 *
 */
package de.brathaerig.chat;

/**
 *
 * @author Tim
 */

import java.sql.*;
import java.math.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class main {
    
    public static void main(String args[]) {
        
        Connection con;
        String dbHost = "hostname"; // Hostname
        String dbPort = "3306";      // Port -- Default: 3306
        String dbName = "database";   // Database Name
        String dbUser = "user";     // Database User
        String dbPass = "Password";      // Database Password
       
        
   
    while (true){
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            con = DriverManager.getConnection("jdbc:mysql://"+dbHost+":"+ dbPort+"/"+dbName+"?"+"user="+dbUser+"&"+"password="+dbPass);
            
           //Get Data from MYSQL Database
            
            Statement query = null;            
                
                query = con.createStatement();   
                
                String sql = "SELECT * from chat";
                
                ResultSet result = query.executeQuery(sql);
                
                while (result.next()){
                    String user = result.getString("user");
                    String text = result.getString("sendText");
                    String output = user + ": " + text;
                    
                    System.out.println(output);
                    
                }
                System.out.println("Enter your Message:");
                
                String user = "Tim";
                Scanner reader = new Scanner(System.in);
                String sendText = (reader.nextLine());
                System.out.println(user + ": " + sendText);
                
                //Send Message
                
                Statement stt = null;
                
                stt = con.createStatement();
                
                String sqlsend = "insert into chat "
                     + " (user, sendText )"
                     + " values ('"+user+"', '"+sendText+"')";
                        
                stt.executeUpdate(sqlsend);
                
               System.out.println("Insert completed");
          
               
               
    } catch (ClassNotFoundException e) {
        System.out.println("Treiber nicht gefunden");
    } catch (SQLException e) {
        System.out.println("Verbindung nicht moglich");
        System.out.println("SQLException: " + e.getMessage());
        System.out.println("SQLState: " + e.getSQLState());
        System.out.println("VendorError: " + e.getErrorCode());
    }   catch (InstantiationException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    }
    
}
